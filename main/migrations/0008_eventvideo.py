# Generated by Django 2.2.7 on 2019-12-11 17:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_eventimage'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventVideo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('yt_id', models.URLField()),
                ('event', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='video', to='main.Event')),
            ],
        ),
    ]

# Generated by Django 2.2.7 on 2019-12-11 17:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_content'),
    ]

    operations = [
        migrations.CreateModel(
            name='CardDesign',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('front', models.ImageField(upload_to='card/%Y/%m/%d')),
                ('back', models.ImageField(upload_to='card/%Y/%m/%d')),
                ('font_color_code', models.CharField(max_length=50)),
            ],
        ),
    ]

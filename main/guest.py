from django.conf import settings

from .models import Event, EventGuest


class Guest(object):
    def __init__(self, request):
        """
        Initialize the cart.
        """
        self.session = request.session
        guest = self.session.get(settings.GUEST_SESSION_ID)
        if not guest:
            # save an empty guest in the session
            guest = self.session[settings.GUEST_SESSION_ID] = {}
        self.guest = guest
        # Check for deleted events
        self.check()

    def check(self):
        for event_pk in list(self.guest):
            try:
                Event.objects.get(pk=int(event_pk))
            except:
                del self.guest[event_pk]

    def add(self, event, event_guest):
        """
        Add guest to the event
        """
        event_pk = str(event.pk)
        event_guest_pk = str(event_guest.pk)
        self.guest[event_pk] = event_guest_pk
        self.save()

    def check_joined(self, event):
        """
        Check guest have joined the event or not
        """
        event_pk = str(event.pk)
        if event_pk in self.guest:
            return int(self.guest[event_pk])
        return None

    def remove(self, guest):
        """
        Remove the guest from event
        """
        event_pk = str(guest.event.pk)
        del self.guest[event_pk]
        self.save()

    def save(self):
        # mark the session as "modified" to make sure it gets saved
        self.session.modified = True

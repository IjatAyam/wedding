from .forms import AuthenticationHomeForm


def login_form(request):
    form = AuthenticationHomeForm()
    return {'login_form': form}

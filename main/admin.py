from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

from .models import Webs, User, Profile, Event, EventImage, EventInvitor, EventActivity, EventGuest, EventVideo, EventMap, Content, CardDesign


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (
            "Personal info",
            {"fields": ("first_name", "last_name")},
        ),
        (
            "Permissions",
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        (
            "Important dates",
            {"fields": ("last_login", "date_joined")},
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2"),
            },
        ),
    )
    list_display = (
        "email",
        "first_name",
        "last_name",
        "is_staff",
    )
    search_fields = ("email", "first_name", "last_name")
    ordering = ("email",)


admin.site.register(Webs)
admin.site.register(Profile)
admin.site.register(Event)
admin.site.register(EventImage)
admin.site.register(EventInvitor)
admin.site.register(EventActivity)
admin.site.register(EventGuest)
admin.site.register(EventVideo)
admin.site.register(EventMap)
admin.site.register(Content)
admin.site.register(CardDesign)

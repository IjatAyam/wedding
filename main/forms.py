from django import forms
from django.contrib.auth.forms import (
    UserCreationForm as DjangoUserCreationForm,
    AuthenticationForm as DjangoAuthenticationForm,
    PasswordChangeForm as DjangoPasswordChangeForm,
    SetPasswordForm as DjangoSetPasswordForm,
)
from django.contrib.auth.forms import UsernameField
from django.core.mail import send_mail

from .models import User, Event, EventActivity, EventGuest, EventVideo, EventMap, EventImage

from bootstrap_datepicker_plus import DatePickerInput, TimePickerInput

import logging

logger = logging.getLogger(__name__)


class UserCreationForm(DjangoUserCreationForm):
    class Meta(DjangoUserCreationForm.Meta):
        model = User
        fields = ("email",)
        field_classes = {"email": UsernameField}

    def send_mail(self):
        logger.info(
            f"Sending signup email for email={self.cleaned_data['email']}")
        message = f"Welcome {self.cleaned_data['email']}"
        send_mail(
            "Welcome to Digital Wedding",
            message,
            "site@booktime.domain",
            [self.cleaned_data['email']],
            fail_silently=True,
        )


class UserCreationHomeForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].help_text = None
        self.fields['password1'].widget = forms.PasswordInput(attrs={
            'autocomplete': 'new-password',
            'placeholder': 'Password*',
        })
        self.fields['password2'].widget = forms.PasswordInput(attrs={
            'autocomplete': 'new-password',
            'placeholder': 'Password confirmation*',
        })
        self.fields['password1'].label = ''
        self.fields['password2'].label = ''

    class Meta(UserCreationForm.Meta):
        widgets = {
            'email': forms.TextInput(attrs={'placeholder': 'Email address*'}),
        }
        labels = {
            'email': ''
        }


class AuthenticationHomeForm(DjangoAuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(DjangoAuthenticationForm, self).__init__(*args, **kwargs)


class PasswordChangeForm(DjangoPasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['new_password1'].help_text = None
        self.fields['old_password'].label = ''
        self.fields['old_password'].widget = forms.PasswordInput(attrs={
            'autocomplete': 'current-password',
            'autofocus': True,
            'placeholder': 'Old password*'
        })
        self.fields['new_password1'].widget = forms.PasswordInput(attrs={
            'autocomplete': 'new-password',
            'placeholder': 'Password*',
        })
        self.fields['new_password2'].widget = forms.PasswordInput(attrs={
            'autocomplete': 'new-password',
            'placeholder': 'Password confirmation*',
        })
        self.fields['new_password1'].label = ''
        self.fields['new_password2'].label = ''


class SetPasswordForm(DjangoSetPasswordForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['new_password1'].help_text = None


class EventForm(forms.ModelForm):
    held_on = forms.DateField(label='Date', input_formats=['%d/%m/%Y'],
                              widget=DatePickerInput(format='%d/%m/%Y'))

    class Meta:
        model = Event
        fields = [
            'held_on',
            'address1',
            'address2',
            'postcode',
            'city',
            'state',
        ]


class ActivityForm(forms.ModelForm):
    class Meta:
        model = EventActivity
        fields = [
            'activity',
            'start',
            'end',
        ]
        widgets = {
            'start': TimePickerInput().start_of('activity time'),
            'end': TimePickerInput().end_of('activity time'),
        }


class EventGuestForm(forms.ModelForm):
    time_come = forms.TimeField(
        label='Estimated time to come', widget=TimePickerInput())

    class Meta:
        model = EventGuest
        fields = [
            'nickname',
            'num_guest',
            'time_come',
        ]


class EventVideoForm(forms.ModelForm):
    yt_id = forms.URLField(label='Youtube URL')

    class Meta:
        model = EventVideo
        fields = [
            'yt_id'
        ]


class EventMapForm(forms.ModelForm):
    map_url = forms.URLField(label='Google Maps URL')

    class Meta:
        model = EventMap
        fields = [
            'map_url'
        ]


class EventImageForm(forms.ModelForm):
    class Meta:
        model = EventImage
        fields = [
            'image',
        ]
        widgets = {
            'image': forms.FileInput(attrs={
                'onchange': 'loadImageFile();',
            })
        }


class TimeRangeForm(forms.Form):
    begin = forms.TimeField(
        label='', widget=TimePickerInput(attrs={'placeholder': 'Begin'}))
    end = forms.TimeField(
        label='', widget=TimePickerInput(attrs={'placeholder': 'End'}))

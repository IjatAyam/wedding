import logging
import datetime

from django.contrib.auth.views import (
    LoginView as AuthLoginView,
    LogoutView as AuthLogoutView,
    PasswordChangeView as AuthPasswordChangeView,
    PasswordChangeDoneView as AuthPasswordChangeDoneView,
    PasswordResetView as AuthPasswordResetView,
    PasswordResetDoneView as AuthPasswordResetDoneView,
    PasswordResetConfirmView as AuthPasswordResetConfirmView,
    PasswordResetCompleteView as AuthPasswordResetCompleteView,
)
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseBadRequest
from django.shortcuts import get_object_or_404, get_list_or_404
from django.contrib.auth import login, authenticate
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, UpdateView, CreateView, DeleteView


from .forms import (
    UserCreationForm,
    UserCreationHomeForm,
    AuthenticationHomeForm,
    EventForm,
    ActivityForm,
    EventGuestForm,
    EventVideoForm,
    EventMapForm,
    PasswordChangeForm,
    SetPasswordForm,
    EventImageForm,
    TimeRangeForm,
)
from .models import (
    Webs,
    Profile,
    Event,
    EventImage,
    EventInvitor,
    EventActivity,
    EventGuest,
    EventVideo,
    EventMap,
    Content,
    CardDesign,
)
from .guest import Guest

logger = logging.getLogger(__name__)


def home_signup(request):
    image = get_object_or_404(Webs, title='home cover image')
    content = get_object_or_404(Content, page='home')
    signup_form = UserCreationHomeForm()
    return render(request, 'main/home.html', {'image': image, 'signup_form': signup_form, 'content': content})


def about(request):
    image = get_object_or_404(Webs, title='home cover image')
    content = get_object_or_404(Content, page='about')
    return render(request, 'main/about.html', {'image': image, 'content': content})


def get_event(groom, bride, pk, user=None):
    if user:
        event = get_object_or_404(
            Event, user__profile__nickname_M__iexact=groom, user__profile__nickname_F__iexact=bride, pk=pk, user=user)
    else:
        event = get_object_or_404(
            Event, user__profile__nickname_M__iexact=groom, user__profile__nickname_F__iexact=bride, pk=pk)
    return event


def event_url(event):
    groom = event.user.profile.nickname_M.lower()
    bride = event.user.profile.nickname_F.lower()
    pk = event.pk
    return [groom, bride, pk]


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = Profile
    fields = [
        "full_name_M",
        "nickname_M",
        "full_name_F",
        "nickname_F",
    ]
    success_url = reverse_lazy('event_list')
    template_name = 'account/profile_form.html'

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)

    def get_object(self):
        return get_object_or_404(Profile, user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context


class SignupView(FormView):
    template_name = 'account/signup.html'
    form_class = UserCreationHomeForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context

    def get_success_url(self):
        redirect_to = self.request.POST.get("next")
        if not redirect_to:
            redirect_to = reverse('event_list')
        return redirect_to

    def form_valid(self, form):
        response = super().form_valid(form)
        form.save()
        email = form.cleaned_data.get("email")
        raw_password = form.cleaned_data.get("password1")
        logger.info(f"New signup for email={email} through SignupView")
        user = authenticate(email=email, password=raw_password)
        login(self.request, user)

        form.send_mail()

        messages.info(self.request, "You signed up successfully.")

        return response


class LoginView(AuthLoginView):
    def get_success_url(self):
        redirect_to = self.request.POST.get("next")
        if not redirect_to:
            redirect_to = reverse('event_list')
        return redirect_to

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context


class LogoutView(AuthLogoutView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context


class PasswordChangeView(AuthPasswordChangeView):
    form_class = PasswordChangeForm
    template_name = 'account/password_change_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context


class PasswordChangeDoneView(AuthPasswordChangeDoneView):
    template_name = 'account/password_change_done.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context


class PasswordResetView(AuthPasswordResetView):
    email_template_name = 'account/password_reset_email.html'
    template_name = 'account/password_reset_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context


class PasswordResetDoneView(AuthPasswordResetDoneView):
    template_name = 'account/password_reset_done.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context


class PasswordResetConfirmView(AuthPasswordResetConfirmView):
    form_class = SetPasswordForm
    template_name = 'account/password_reset_confirm.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context


class PasswordResetCompleteView(AuthPasswordResetCompleteView):
    template_name = 'account/password_reset_complete.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["image"] = get_object_or_404(Webs, title='home cover image')
        return context


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventForm

    def dispatch(self, request, *args, **kwargs):
        profile = Profile.objects.get(user=self.request.user)
        if profile.full_name_M == '':
            return redirect('profile_update')
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        self.event = obj
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        event = Event.objects.get(pk=self.event.pk)
        return reverse('event_detail', args=event_url(event))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["create"] = "active"
        context["title"] = "Create Event"
        return context


class EventUpdateView(LoginRequiredMixin, UpdateView):
    model = Event
    form_class = EventForm

    def dispatch(self, request, *args, **kwargs):
        self.event = get_object_or_404(
            Event, pk=self.kwargs['pk'], user=self.request.user)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        return reverse('event_detail', args=event_url(self.event))


class EventImageCreateView(LoginRequiredMixin, CreateView):
    model = EventImage
    form_class = EventImageForm

    def dispatch(self, request, *args, **kwargs):
        self.event = get_object_or_404(
            Event, pk=self.kwargs['pk'], user=self.request.user)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.event = self.event
        obj.save()
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('event_detail', args=event_url(self.event))


class EventImageDeleteView(LoginRequiredMixin, DeleteView):
    model = EventImage

    def get_success_url(self, **kwargs):
        return reverse('event_detail', args=event_url(self.object.event))


class EventInvitorCreateView(LoginRequiredMixin, CreateView):
    model = EventInvitor
    fields = [
        'full_name',
        'nickname',
        'relation',
        'phone_no1',
        'phone_no2',
    ]

    def dispatch(self, request, *args, **kwargs):
        self.event = get_object_or_404(
            Event, pk=self.kwargs['pk'], user=self.request.user)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.event = self.event
        obj.save()
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('event_detail', args=event_url(self.event))


class EventInvitorDeleteView(LoginRequiredMixin, DeleteView):
    model = EventInvitor

    def get_success_url(self, **kwargs):
        return reverse('event_detail', args=event_url(self.object.event))


class ActivityCreateView(LoginRequiredMixin, CreateView):
    model = EventActivity
    form_class = ActivityForm

    def dispatch(self, request, *args, **kwargs):
        self.event = get_object_or_404(
            Event, pk=self.kwargs['pk'], user=self.request.user)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.event = self.event
        obj.save()
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('activity_list', args=event_url(self.event))


class ActivityDeleteView(LoginRequiredMixin, DeleteView):
    model = EventActivity

    def dispatch(self, request, *args, **kwargs):
        self.activity = get_object_or_404(
            EventActivity, pk=self.kwargs['pk'], event__user=self.request.user)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        return reverse('activity_list', args=event_url(self.activity.event))


class ActivityListView(LoginRequiredMixin, ListView):
    model = EventActivity
    allow_empty = False

    def get_queryset(self):
        qs = super().get_queryset()
        event = get_event(self.kwargs['groom'],
                          self.kwargs['bride'], self.kwargs['pk'])
        return qs.filter(event__pk=event.pk, event__user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        event = Event.objects.get(pk=self.kwargs['pk'])
        context["event_pk"] = event.pk
        context["groom"] = event.user.profile.nickname_M.lower()
        context["bride"] = event.user.profile.nickname_F.lower()
        return context


class EventListView(LoginRequiredMixin, ListView):
    model = Event

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["list"] = "active"
        context["no_image"] = get_object_or_404(Webs, title='no-image')
        return context

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class EventDetailView(LoginRequiredMixin, DetailView):
    model = Event

    def get_object(self):
        obj = get_event(self.kwargs['groom'],
                        self.kwargs['bride'], self.kwargs['pk'])
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['groom'] = self.kwargs['groom'].lower()
        context['bride'] = self.kwargs['bride'].lower()
        return context

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class GuestEventDetailView(DetailView):
    model = Event
    template_name = 'main/guestevent_detail.html'

    def get_object(self):
        obj = get_event(self.kwargs['groom'],
                        self.kwargs['bride'], self.kwargs['pk'])
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.guest = Guest(self.request)
        guest_pk = self.guest.check_joined(self.object)
        if guest_pk:
            guest = EventGuest.objects.get(pk=guest_pk)
        else:
            guest = None
        context['guest'] = guest
        context['groom'] = self.kwargs['groom'].lower()
        context['bride'] = self.kwargs['bride'].lower()
        return context


class GuestActivityListView(ListView):
    model = EventActivity
    template_name = 'main/guesteventactivity_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["event"] = get_object_or_404(Event, pk=self.kwargs['pk'])
        context['groom'] = self.kwargs['groom'].lower()
        context['bride'] = self.kwargs['bride'].lower()
        context["frame"] = get_object_or_404(Webs, title='wedding-frame')
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        event = get_event(self.kwargs['groom'],
                          self.kwargs['bride'], self.kwargs['pk'])
        return qs.filter(
            event__pk=event.pk)


class EventGuestCreateView(CreateView):
    model = EventGuest
    form_class = EventGuestForm

    def dispatch(self, request, *args, **kwargs):
        self.guest = Guest(self.request)
        self.event = get_event(self.kwargs['groom'],
                               self.kwargs['bride'], self.kwargs['pk'])
        guest_pk = self.guest.check_joined(self.event)
        if guest_pk:
            return redirect(reverse('guest_update', args=[guest_pk]))
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.event = self.event
        obj.save()
        self.guest.add(self.event, obj)
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('guest_event_detail', args=event_url(self.event))


class EventGuestUpdateView(UpdateView):
    model = EventGuest
    form_class = EventGuestForm

    def get_object(self):
        event = get_event(self.kwargs['groom'],
                          self.kwargs['bride'], self.kwargs['event_pk'])
        obj = get_object_or_404(EventGuest, pk=self.kwargs['pk'], event=event)
        return obj

    def get_success_url(self, **kwargs):
        return reverse('guest_event_detail', args=event_url(self.object.event))


class EventGuestDeleteView(DeleteView):
    model = EventGuest

    def get_object(self):
        event = get_event(self.kwargs['groom'],
                          self.kwargs['bride'], self.kwargs['event_pk'])
        obj = get_object_or_404(EventGuest, pk=self.kwargs['pk'], event=event)
        return obj

    def get_success_url(self, **kwargs):
        guest = Guest(self.request)
        guest.remove(self.object)
        return reverse('guest_event_detail', args=event_url(self.object.event))


class GuestListView(LoginRequiredMixin, ListView):
    model = EventGuest
    allow_empty = True
    ordering = ['time_come']

    def dispatch(self, request, *args, **kwargs):
        self.event = get_event(self.kwargs['groom'],
                               self.kwargs['bride'], self.kwargs['pk'])
        self.begin = None
        self.end = None
        if self.request.GET.get("begin") and self.request.GET.get("end"):
            self.begin = datetime.datetime.strptime(
                self.request.GET.get("begin"), '%H:%M').time()
            self.end = datetime.datetime.strptime(
                self.request.GET.get("end"), '%H:%M').time()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["guest_count"] = self.guest_count
        context["range_form"] = TimeRangeForm()
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(
            event__pk=self.event.pk, event__user=self.request.user)
        if self.begin and self.end:
            qs = qs.filter(time_come__range=(self.begin, self.end))
        self.guest_count = sum(i.num_guest for i in qs)
        return qs


@login_required
def card_choose(request, groom, bride, pk):
    event = get_event(groom, bride, pk, user=request.user)
    card_list = CardDesign.objects.all()
    event_link = request.scheme + "://" + request.META['HTTP_HOST'] + \
        reverse('guest_event_detail', args=event_url(event))

    for idx, card in enumerate(card_list):
        if request.GET.get("type") == str(idx+1):
            if request.GET.get("card") == "front":
                return render(request, "card/card_front.html", {"image": card, "event": event})

            if request.GET.get("card") == "back":
                return render(request, "card/card_back.html", {"image": card, "event": event, "event_link": event_link})

    context = {
        "event": event,
        "card_list": card_list,
        "event_link": event_link,
        "groom": groom.lower(),
        "bride": bride.lower(),
    }
    return render(request, "main/card_choose.html", context)


@login_required
def event_add_video(request, event_pk):
    event = get_object_or_404(Event, pk=event_pk, user=request.user)

    if request.method == 'POST':
        form = EventVideoForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.event = event
            obj.save()
            args = [arg for arg in event_url(event)]
            return redirect('event_detail', args[0], args[1], args[2])

    else:
        form = EventVideoForm()

    return render(request, 'main/eventvideo_form.html', {'form': form, 'event': event})


@login_required
def event_delete_video(request, event_pk):
    event = get_object_or_404(Event, pk=event_pk, user=request.user)
    video = get_object_or_404(EventVideo, event=event)
    video.delete()
    args = [arg for arg in event_url(event)]
    return redirect('event_detail', args[0], args[1], args[2])


@login_required
def event_add_map(request, event_pk):
    event = get_object_or_404(Event, pk=event_pk, user=request.user)

    if request.method == 'POST':
        form = EventMapForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.event = event
            obj.save()
            args = [arg for arg in event_url(event)]
            return redirect('event_detail', args[0], args[1], args[2])

    else:
        form = EventMapForm()

    return render(request, 'main/eventmap_form.html', {'form': form, 'event': event})


@login_required
def event_delete_map(request, event_pk):
    event = get_object_or_404(Event, pk=event_pk, user=request.user)
    map_url = get_object_or_404(EventMap, event=event)
    map_url.delete()
    args = [arg for arg in event_url(event)]
    return redirect('event_detail', args[0], args[1], args[2])

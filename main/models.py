from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.core.validators import MinValueValidator
from django.db import models

from PIL import Image

import logging

logger = logging.getLogger(__name__)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = None
    email = models.EmailField('email address', unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()


class Webs(models.Model):
    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to='webs/%Y/%m/%d')

    class Meta:
        verbose_name_plural = "webs"

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.image.path)

        if img.height > 1080 or img.width > 720:
            new_img = (1080, 720)
            img.thumbnail(new_img)
            img.save(self.image.path)

    def __str__(self):
        return self.title


class Content(models.Model):
    page = models.CharField(max_length=20)
    title = models.CharField(max_length=200)
    content = models.TextField(blank=True)

    def __str__(self):
        return self.title


class CardDesign(models.Model):
    name = models.CharField(max_length=200)
    front = models.ImageField(upload_to='card/%Y/%m/%d')
    back = models.ImageField(upload_to='card/%Y/%m/%d')
    font_color_code = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img_back = Image.open(self.back.path)

        if img_back.height > 1080 or img_back.width > 720:
            new_img_back = (1080, 720)
            img_back.thumbnail(new_img_back)
            img_back.save(self.back.path)

        img_front = Image.open(self.front.path)

        if img_front.height > 1080 or img_front.width > 720:
            new_img_front = (1080, 720)
            img_front.thumbnail(new_img_front)
            img_front.save(self.front.path)


class Profile(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name="profile")
    full_name_M = models.CharField("Full Name(Male)", max_length=200)
    nickname_M = models.CharField("Nickname(Male)", max_length=50)
    full_name_F = models.CharField("Full Name(Female)", max_length=200)
    nickname_F = models.CharField("Nickname(Female)", max_length=50)

    def __str__(self):
        return f"Profile for {self.user.email}"


class Event(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='event')
    held_on = models.DateField("Date")
    address1 = models.CharField("Address line 1", max_length=60)
    address2 = models.CharField("Address line 2", max_length=60, blank=True)
    postcode = models.CharField(max_length=12)
    city = models.CharField(max_length=60)
    state = models.CharField(max_length=30)

    def guest_count(self):
        return sum(i.num_guest for i in self.guest.all())

    def image_count(self):
        return self.eventimage_set.all().count()

    def invitor_count(self):
        return self.invitor.all().count()

    def __str__(self):
        return f"{self.user.email}'s event"


class EventImage(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='events/%Y/%m/%d')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.image.path)

        if img.height > 1080 or img.width > 720:
            new_img = (1080, 720)
            img.thumbnail(new_img)
            img.save(self.image.path)

    def __str__(self):
        return f"{self.event}'s image"


class EventVideo(models.Model):
    event = models.OneToOneField(
        Event, on_delete=models.CASCADE, related_name='video')
    yt_id = models.URLField(max_length=200)

    def __str__(self):
        return f"{self.event}'s video"


class EventMap(models.Model):
    event = models.OneToOneField(
        Event, on_delete=models.CASCADE, related_name='map')
    map_url = models.URLField(max_length=200)

    def __str__(self):
        return f"{self.event}'s map"


class EventInvitor(models.Model):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE, related_name='invitor')
    full_name = models.CharField(max_length=200)
    nickname = models.CharField(max_length=50, blank=True)
    relation = models.CharField(max_length=20)
    phone_no1 = models.CharField('Phone Number', max_length=20)
    phone_no2 = models.CharField(
        'Phone Number 2 (optional)', max_length=20, blank=True)


class EventActivity(models.Model):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE, related_name='activity')
    activity = models.CharField(max_length=100)
    start = models.TimeField()
    end = models.TimeField()

    class Meta:
        verbose_name_plural = "event activities"


class EventGuest(models.Model):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE, related_name='guest')
    nickname = models.CharField(max_length=50)
    num_guest = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1)])
    time_come = models.TimeField()

    def __str__(self):
        return self.nickname

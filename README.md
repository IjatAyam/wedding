# Digital Wedding

## A digital wedding invitation web apps

### Get started:

Install python 3.7.x and PostgreSQL in your machine.

Install pipenv:

```bash
  pip install pipenv
```

cd to folder with `manage.py` file, then install package from pipenv

```bash
  pipenv install --ignore-pipfile
```

create new database in your PostgreSQL, edit the settings.py in folder `wedding`:

```python
  DATABASES = {
      'default': {
          'ENGINE': 'django.db.backends.postgresql',
          'NAME': '{your-db-name}',
          'USER': '{your-db-user}',
          'PASSWORD': '{your-db-password}',
      }
  }
```

migrate model to database:

```bash
  python manage.py migrate
```
